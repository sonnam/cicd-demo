import os
import sys
import time
from collections import OrderedDict

### For RESTCONF
import requests
import json


class Usefull_Skills:
    def setup(self):
        self.url = ""
        self.response = ""

    # Function to retrieve the lcurrent ARP table
    def get_arp(self, url_base, headers, username, password):
        self.url = url_base + "/data/Cisco-IOS-XE-arp-oper:arp-data/"

        self.response = requests.get(
            self.url, auth=(username, password), headers=headers, verify=False
        )

        # return the json as text
        return self.response.json()["Cisco-IOS-XE-arp-oper:arp-data"]["arp-vrf"][0][
            "arp-oper"
        ]

    # Function to retrieve system info
    def get_sys_info(self, url_base, headers, username, password):
        self.url = (
            url_base + "/data/Cisco-IOS-XE-device-hardware-oper:device-hardware-data/"
        )

        self.response = requests.get(
            self.url, auth=(username, password), headers=headers, verify=False
        )

        # return the json as text
        return self.response.json()[
            "Cisco-IOS-XE-device-hardware-oper:device-hardware-data"
        ]["device-hardware"]

    # Function to retrieve the list of interfaces on a device
    def get_configured_interfaces(self, url_base, headers, username, password):
        self.url = url_base + "/data/ietf-interfaces:interfaces"

        self.response = requests.get(
            self.url, auth=(username, password), headers=headers, verify=False
        )
        # return the json as text
        return self.response.json()["ietf-interfaces:interfaces"]["interface"]

    # Used to configure the IP address on an interface
    def configure_ip_address(
        self, interface, ip, mask, url_base, headers, username, password
    ):
        # RESTCONF URL for specific interface
        self.url = url_base + "/data/ietf-interfaces:interfaces/interface={i}".format(
            i=interface
        )

        type = "iana-if-type:ethernetCsmacd"
        if "Loopback" in interface:
            type = "iana-if-type:softwareLoopback"

        # Create the data payload to reconfigure IP address
        # Need to use OrderedDicts to maintain the order of elements
        data = OrderedDict(
            [
                (
                    "ietf-interfaces:interface",
                    OrderedDict(
                        [
                            ("name", interface),
                            ("type", type),
                            (
                                "ietf-ip:ipv4",
                                OrderedDict(
                                    [
                                        (
                                            "address",
                                            [
                                                OrderedDict(
                                                    [
                                                        ("ip", ip),
                                                        ("netmask", mask),
                                                    ]
                                                )
                                            ],
                                        )
                                    ]
                                ),
                            ),
                        ]
                    ),
                )
            ]
        )

        # Use PUT request to update data
        self.response = requests.put(
            self.url,
            auth=(username, password),
            headers=headers,
            verify=False,
            json=data,
        )
        return self.response.status_code

    # Used to configure the IP address on an interface
    def delete_ip_address(self, interface, ip, url_base, headers, username, password):
        # RESTCONF URL for specific interface
        self.url = (
            url_base
            + "/data/ietf-interfaces:interfaces/interface={i}/ipv4/address={j}".format(
                i=interface, j=ip
            )
        )
        # Use PUT request to update data
        self.response = requests.delete(
            self.url, auth=(username, password), headers=headers, verify=False
        )
        return self.response.status_code

    # This function add network to BGP
    def add_net_to_bgp(self, prefix, mask, url_base, headers, username, password):
        # RESTCONF URL for specific interface
        self.url = (
            url_base
            + "/data/native/router/Cisco-IOS-XE-bgp:bgp=65001/address-family/no-vrf/ipv4=unicast/ipv4-unicast/network/with-mask"
        )
        data = OrderedDict(
            [
                (
                    "Cisco-IOS-XE-bgp:with-mask",
                    OrderedDict([("number", prefix), ("mask", mask)]),
                )
            ]
        )

        # Use patch request to update data
        self.response = requests.patch(
            self.url,
            auth=(username, password),
            headers=headers,
            verify=False,
            json=data,
        )
        return self.response.status_code

    # This function revoce a network from BGP
    def rmv_net_from_bgp(self, prefix, mask, url_base, headers, username, password):
        # RESTCONF URL for specific interface
        self.url = (
            url_base
            + "/data/native/router/Cisco-IOS-XE-bgp:bgp=65001/address-family/no-vrf/ipv4=unicast/ipv4-unicast/network/with-mask={i},{j}".format(
                i=prefix, j=mask
            )
        )
        # Use delete request to update data
        self.response = requests.delete(
            self.url,
            auth=(username, password),
            headers=headers,
            verify=False,
        )
        return self.response.status_code


if __name__ == "__main__":
    import routers

    # Router Info
    interface = "Loopback3"
    ip = "11.1.2.1"
    prefix = "11.1.2.0"
    mask = "255.255.255.0"
    device_address = routers.router["host"]
    device_username = routers.router["username"]
    device_password = routers.router["password"]
    # RESTCONF Setup
    port = "443"
    url_base = "https://{h}/restconf".format(h=device_address)
    headers = {
        "Content-Type": "application/yang-data+json",
        "Accept": "application/yang-data+json",
    }
    u = Usefull_Skills()

    intf_list = u.get_configured_interfaces(
        url_base, headers, device_username, device_password
    )
    for intf in intf_list:
        print("Name:{}".format(intf["name"]))
        try:
            print(
                "IP Address:{}\{}\n".format(
                    intf["ietf-ip:ipv4"]["address"][0]["ip"],
                    intf["ietf-ip:ipv4"]["address"][0]["netmask"],
                )
            )
        except KeyError:
            print("IP Address: UNCONFIGURED\n")

    print(
        u.configure_ip_address(
            interface, ip, mask, url_base, headers, device_username, device_password
        )
    )
    print("/n")
    print(
        u.add_net_to_bgp(
            prefix, mask, url_base, headers, device_username, device_password
        )
    )
    time.sleep(2)
    print("/n")
    print(
        u.rmv_net_from_bgp(
            prefix, mask, url_base, headers, device_username, device_password
        )
    )
    print("/n")
    print(
        u.delete_ip_address(
            interface, ip, url_base, headers, device_username, device_password
        )
    )
