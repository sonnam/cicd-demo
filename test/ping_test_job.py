import os
import argparse
from pyats.easypy import run

# command line argument parser
parser = argparse.ArgumentParser()
parser.add_argument(
    "--dest",
    dest="ping_list",
    type=str,
    default="1.1.1.1 11.1.1.1 12.1.1.1 2.2.2.2 11.2.2.2 12.2.2.2",
    help="space delimted list of IP address(es) to test connectivity",
)
# compute the script path from this location
SCRIPT_PATH = os.path.dirname(__file__)


def main(runtime):
    """job file entrypoint"""
    # parse command line arguments
    # only parse arguments we know
    args, _ = parser.parse_known_args()

    # run script, pass arguments to script as parameters
    run(
        testscript=os.path.join(SCRIPT_PATH, "Ping_Test.py"),
        runtime=runtime,
        taskid="Ping",
        **vars(args)
    )
