#!/bin/env python

# To get a log for the script
import logging
import json

# To build the table at the end
from tabulate import tabulate

# Needed for aetest script
from pyats import aetest
from pyats.log.utils import banner

# Genie Imports
from genie.conf import Genie
from genie.abstract import Lookup

# import the genie libs
from genie.libs import ops  # noqa
from genie.testbed import load
from unicon.core.errors import TimeoutError, StateMachineError, ConnectionError

# create a log for this module
log = logging.getLogger(__name__)

# List of addresses to ping:
# ping_list = ['208.67.222.222', '8.8.8.8', '1.1.1.1']

###################################################################
#                  COMMON SETUP SECTION                           #
###################################################################


class CommonSetup(aetest.CommonSetup):
    """Common Setup section"""

    # CommonSetup have subsection.
    # You can have 1 to as many subsection as wanted

    # Connect to each device in the testbed
    @aetest.subsection
    def connect(self, testbed):
        genie_testbed = Genie.init(testbed)
        self.parent.parameters["testbed"] = genie_testbed
        device_list = []
        for device in genie_testbed.devices.values():
            log.info(banner("Connect to device '{d}'".format(d=device.name)))
            try:
                device.connect()
            except Exception as e:
                self.failed(
                    "Failed to establish connection to '{}'".format(device.name)
                )

            device_list.append(device)

        # Pass list of devices the to testcases
        self.parent.parameters.update(dev=device_list)


###################################################################
#                     TESTCASES SECTION                           #
###################################################################


class ping_class(aetest.Testcase):
    @aetest.setup
    def setup(self, testbed, ping_list):
        """Make sure devices can ping a list of addresses."""
        # Create an array of destination IPs from our argparse
        ping_list = ping_list.split()

        self.ping_results = {}
        for device_name, device in testbed.devices.items():
            # Only attempt to ping on supported network operation systems
            if device.os in ("ios", "iosxe", "iosxr", "nxos"):
                log.info(f"{device_name} connected status: {device.connected}")
                self.ping_results[device_name] = {}
                for ip in ping_list:
                    log.info(f"Pinging {ip} from {device_name}")
                    try:
                        ping = device.ping(ip)
                        pingSuccessRate = ping[
                            (ping.find("percent") - 4) : ping.find("percent")
                        ].strip()
                        try:
                            self.ping_results[device_name][ip] = int(pingSuccessRate)
                        except:
                            self.ping_results[device_name][ip] = 0
                            self.failed("Ping Failed")
                    except:
                        self.ping_results[device_name][ip] = 0
                        self.failed("Ping Failed")

    @aetest.test
    def test(self, steps):
        # Loop over every ping result
        for device_name, ips in self.ping_results.items():
            with steps.start(
                f"Looking for ping failures {device_name}", continue_=True
            ) as device_step:
                # Loop over every ping result
                for ip in ips:
                    with device_step.start(
                        f"Checking Ping from {device_name} to {ip}", continue_=True
                    ) as ping_step:
                        if ips[ip] < 100:
                            device_step.failed(
                                f"Device {device_name} had {ips[ip]}% success pinging {ip}"
                            )


class CommonCleanup(aetest.CommonCleanup):
    # CommonCleanup follow exactly the same rule as CommonSetup regarding
    # subsection
    # You can have 1 to as many subsections as wanted
    # here is an example of 1 subsection

    @aetest.subsection
    def clean_everything(self):
        """Common Cleanup Subsection"""
        log.info("Aetest Common Cleanup ")


if __name__ == "__main__":
    # for stand-alone execution
    import argparse
    from pyats import topology

    # from genie.conf import Genie

    parser = argparse.ArgumentParser(description="standalone parser")
    parser.add_argument(
        "--testbed",
        dest="testbed",
        help="testbed YAML file",
        type=topology.loader.load,
        # type=Genie.init,
        default=None,
    )
    parser.add_argument(
        "--dest",
        dest="ping_list",
        type=str,
        default="208.67.222.222 8.8.8.8 8.8.4.4",
        help="space delimted list of IP address(es) to test connectivity",
    )

    # do the parsing
    args = parser.parse_known_args()[0]

    aetest.main(testbed=args.testbed, ping_list=args.ping_list)
