import unittest
import json
import routers
import os
import sys
import time
from collections import OrderedDict

### For RESTCONF
import requests
import json

# test subject
from useful_skills import Usefull_Skills


class UsefulTest(unittest.TestCase):
    def setUp(self):
        self.useful = Usefull_Skills()
        # Router Info
        self.interface = "Loopback3"
        self.ip = "11.1.2.1"
        self.prefix = "11.1.2.0"
        self.mask = "255.255.255.0"
        self.device_address = routers.router["host"]
        self.device_username = routers.router["username"]
        self.device_password = routers.router["password"]
        # RESTCONF Setup
        self.port = "443"
        self.url_base = "https://{h}/restconf".format(h=self.device_address)
        self.headers = {
            "Content-Type": "application/yang-data+json",
            "Accept": "application/yang-data+json",
        }

    def test_1_add_new_ip(self):
        # add new IP address
        time.sleep(1)
        r = self.useful.configure_ip_address(
            self.interface,
            self.ip,
            self.mask,
            self.url_base,
            self.headers,
            self.device_username,
            self.device_password,
        )
        self.assertTrue(r == 204 or r == 201)

    def test_2_add_net_to_bgp(self):
        # add new net to bgp
        time.sleep(2)
        r = self.useful.add_net_to_bgp(
            self.prefix,
            self.mask,
            self.url_base,
            self.headers,
            self.device_username,
            self.device_password,
        )
        self.assertTrue(r == 204 or r == 201)

    def test_3_delete_net_from_bgp(self):
        # delete a net from bgb
        time.sleep(2)
        r = self.useful.rmv_net_from_bgp(
            self.prefix,
            self.mask,
            self.url_base,
            self.headers,
            self.device_username,
            self.device_password,
        )
        self.assertTrue(r == 204 or r == 201)

    def test_4_delete_ip(self):
        # delete an IP address
        time.sleep(2)
        r = self.useful.delete_ip_address(
            self.interface,
            self.ip,
            self.url_base,
            self.headers,
            self.device_username,
            self.device_password,
        )
        self.assertTrue(r == 204 or r == 201)
