### teams Bot ###
from webexteamsbot import TeamsBot
from webexteamsbot.models import Response

import os
import sys
from dotenv import load_dotenv
from requests_toolbelt.multipart.encoder import MultipartEncoder
import json
import requests

load_dotenv()
# Fill in your Teams Bot Token
teams_token = os.getenv("BOT_TOKEN")
# Fill in your Room ID
rid = os.getenv("ROOM_ID")


def create_message(rid, msgtxt):
    headers = {
        "content-type": "application/json; charset=utf-8",
        "authorization": "Bearer " + teams_token,
    }

    url = "https://api.ciscospark.com/v1/messages"
    data = {"roomId": rid, "attachments": [], "markdown": msgtxt}
    response = requests.post(url, json=data, headers=headers)
    return response.json()


def create_message_with_attachment(rid, msgtxt, attachment):
    url = "https://api.ciscospark.com/v1/messages"
    m = MultipartEncoder(
        {
            "roomId": rid,
            "markdown": msgtxt,
            "files": (attachment, open(attachment, "rb"), "image/png"),
        }
    )
    headers = {
        "content-type": "application/json; charset=utf-8",
        "authorization": "Bearer " + teams_token,
        "Content-Type": m.content_type,
    }
    response = requests.post(url, data=m, headers=headers)
    return response.json()


# An example command the illustrates using details from incoming message within
# the command processing.
def current_time(incoming_msg):
    """
    Sample function that returns the current time for a provided timezone
    :param incoming_msg: The incoming message object from Teams
    :return: A Response object based reply
    """

    # Craft REST API URL to retrieve current time
    #   Using API from http://worldclockapi.com
    u = "http://worldclockapi.com/api/json/cst/now"
    r = requests.get(u).json()

    # If an invalid timezone is provided, the serviceResponse will include
    # error message
    if r["serviceResponse"]:
        return "Error: " + r["serviceResponse"]

    # Format of returned data is "YYYY-MM-DDTHH:MM<OFFSET>"
    #   Example "2018-11-11T22:09-05:00"
    returned_data = r["currentDateTime"].split("T")
    cur_date = returned_data[0]
    cur_time = returned_data[1][:5]
    timezone_name = r["timeZoneName"]

    # Craft a reply string.
    reply = "In {TZ} it is currently {TIME} on {DATE}.".format(
        TZ=timezone_name, TIME=cur_time, DATE=cur_date
    )
    return reply


if __name__ == "__main__":
    # create_message_with_attachment(rid, "CI/CD", "./chatbotimo.jpg")

    argv = sys.argv
    if len(argv) == 2:
        create_message(rid, argv[1])
    elif len(argv) == 3:
        create_message_with_attachment(rid, argv[1], argv[2])
