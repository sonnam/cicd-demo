# cicd-NetAutomation-demo

Use Gitlab to create a CI-CD pipeline for updating new network for a BGP topology. The core are composed of 2 IOS-XE routers.  

## Prerequisites

This lab requires Python3 and [git](https://git-scm.com/) installed on your devbox, a [gitlab's account](https://gitlab.com) and two IOS-XE routers.


## Network topology

The network topology for both test and prod are idenyical. 
![network](./images/topo.png)

## Pipeline Technology 

![pipeline](./images/pipeline.png "CICD Pipeline")

## Setup

The following steps are required for the initial installation:

`Configure the pipeline runner:`
Create a project on Gitlab.

This lab, we use gilab-runner locally to execute the pipline script. Follow this [Gitlab-runner installation](https://docs.gitlab.com/runner/install/linux-manually.html) to install it on your testbox and [register](https://docs.gitlab.com/runner/register/index.html) it to your Gitlab project. 

You will want to turn off the share runner on Gitlab since we are using a local runner: Settings->CD/CD->Runners-> toggle off the `Enable shared runners for this project`

By default your Gitlab-runner will create and use gitlab-runner username. The pipeline requires SSH from the runner during validation stage  You might want to copy your `~/.ssh/` folder to the gitlab-runner's home derectory to avoid connection problem with the routers (assume that you have successfully SSH to the routers before copying `.ssh`) 

`Example:` Here, our devbox and testbox are the same machine.
```
cp -r ~/.ssh/* /home/gitlab-runner/
```

Use the command below for dependencies installation. 
```
pip3 install -r requirements.txt
```

## Getting started

You should be able to clone this repository to your local devbox using `git clone` command.

Initalize your repository by `git init` then connect to your remote-repo (your Gitlab project created above) by `git remote add`. 

`Note:` Since we are in developing phase, it is a good idea to push your code to a `dev` branch (`git push`)

Now you can sit back and watch the `CI/CD` in action. 

![Demo](./images/CICD-Demo.gif "CICD")
