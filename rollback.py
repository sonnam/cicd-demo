from useful_skills import Usefull_Skills
import routers
import time

if __name__ == "__main__":

    # Router Info
    interface = "Loopback3"
    ip = "11.1.2.1"
    prefix = "11.1.2.0"
    mask = "255.255.255.0"
    device_address = routers.router["host"]
    device_username = routers.router["username"]
    device_password = routers.router["password"]
    # RESTCONF Setup
    port = "443"
    url_base = "https://{h}/restconf".format(h=device_address)
    headers = {
        "Content-Type": "application/yang-data+json",
        "Accept": "application/yang-data+json",
    }
    u = Usefull_Skills()

    print(
        u.rmv_net_from_bgp(
            prefix, mask, url_base, headers, device_username, device_password
        )
    )
    print("/n")
    time.sleep(2)
    print(
        u.delete_ip_address(
            interface, ip, url_base, headers, device_username, device_password
        )
    )
